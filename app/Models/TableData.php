<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TableData extends Model
{
    use HasFactory;

    protected $fillable = ['table_id', 'x', 'y', 'value'];

    public function table()
    {
        return $this->belongsTo(Table::class);
    }
}
