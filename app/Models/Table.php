<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'user_id'];

    public function cells()
    {
        return $this->hasMany(TableData::class);
    }

    public function getCell($x, $y)
    {
        return $this->cells->where('x', '=', $x)->where('y', '=', $y)->first();
    }

    public function saveCell($x, $y, $value)
    {
        $x = (int)$x;
        $y = (int)$y;

        if (empty($x) || empty($y)) {
            return false;
        }

        $find = $this->getCell($x, $y);

        if (!$find) {
            $find = new TableData();
            $find->table_id = $this->id;
        }

        $find->x = $x;
        $find->y = $y;

        $find->value = $value;

        $find->save();

    }
}
