<?php


namespace App\API;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="OpenApi",
 *      description="Описание API сервиса электронных таблиц",
 * )
 *
 * @OA\Get(
 *      path="/api/table",
 *      operationId="getTablesList",
 *      tags={"Таблицы"},
 *      summary="Список таблиц",
 *      description="Получение списка таблиц авторизованного пользователя",
 *      @OA\Response(response=200, description="Ok"),
 *      @OA\Response(response=401, description="Unauthorized"),
 * )
 *
 * @OA\Get(
 *      path="/api/table/{id}",
 *      operationId="getTable",
 *      tags={"Таблицы"},
 *      summary="Данные таблицы",
 *      description="Получеить данные таблицы",
 *      @OA\Parameter(
 *         description="ID таблицы",
 *         in="path",
 *         name="id",
 *         required=true,
 *         @OA\Schema(
 *           type="integer",
 *           format="int64"
 *         )
 *      ),
 *      @OA\Response(response=200, description="Ok"),
 *      @OA\Response(response=401, description="Unauthorized"),
 *      @OA\Response(response=403, description="Forhiben"),
 *      @OA\Response(response=404, description="Not found"),
 * )
 * @OA\Put(
 *      path="/api/table/{id}",
 *      operationId="putTable",
 *      tags={"Таблицы"},
 *      summary="Обновить данные",
 *      description="Обновить данные таблицы - названия и содержимое ячеек",
 *      @OA\Parameter(
 *         description="ID таблицы",
 *         in="path",
 *         name="id",
 *         required=true,
 *         @OA\Schema(
 *           type="integer",
 *           format="int64"
 *         )
 *      ),
 *      @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Название таблицы",
 *         required=false,
 *         @OA\Schema(
 *           type="string",
 *         )
 *      ),
 *      @OA\Response(response=200, description="Ok"),
 *      @OA\Response(response=401, description="Unauthorized"),
 *      @OA\Response(response=403, description="Forhiben"),
 *      @OA\Response(response=404, description="Not found"),
 * )
 *
 * @OA\Post(
 *      path="/api/table",
 *      tags={"Таблицы"},
 *      summary="Создание таблицы",
 *      description="Создает пустую таблицу для авторизованного пользователя",
 *      @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Название таблицы",
 *         required=false,
 *         @OA\Schema(
 *           type="string",
 *         )
 *      ),
 *      @OA\Response(response=201, description="Created"),
 *      @OA\Response(response=401, description="Unauthorized"),
 * )
 *
 *
 * @OA\Delete(
 *     path="/api/table/{id}",
 *     summary="Удалить таблицу",
 *     description="Удаление таблицы",
 *     operationId="deleteTable",
 *     tags={"Таблицы"},
 *     @OA\Parameter(
 *         description="id",
 *         in="path",
 *         name="id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int64"
 *         )
 *     ),
 *      @OA\Response(response=200, description="Ok"),
 *      @OA\Response(response=401, description="Unauthorized"),
 *      @OA\Response(response=403, description="Forhiben"),
 *      @OA\Response(response=404, description="Not found"),
 * )
 *
 *
 * */

class Documentation
{

}
