<?php

namespace App\API;

use App\Http\Controllers\Controller;
use App\Models\Table;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TableController extends Controller
{

    /**
     * Получение списка таблиц
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $request->user()->tables()->get();
    }

    /**
     * Создаем новую таблицу
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return Table::create(['name' => $request->input('name', ''), 'user_id' => $request->user()->id]);
    }

    /**
     * Получить данные таблицы (ячейки)
     *
     * @param int $id
     * @return Table
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {

        $table = Table::with('cells')->findOrFail($id);

        //защита от просмотра чужих таблиц
        if ($table->user_id != $request->user()->id) {
            throw new \Exception(403);
        }

        return $table;
    }

    /**
     * Обновить данные таблиц
     *
     * @param Request $request
     * @param Table $table
     * @return Table
     * @throws \Exception
     */
    public function update(Request $request, Table $table)
    {
        //защита от изменения чужих таблиц
        if ($table->user_id != $request->user()->id) {
            throw new \Exception(403);
        }

        //Изменяем название
        if ($request->exists('name')) {
            $table->name = $request->input('name');
            $table->save();
        }

        if ($request->exists('cells')) {
            $cells = $request->input('cells');

            foreach ($cells as $cell) {
                $x = (int)$cell['x'];
                $y = (int)$cell['y'];

                if (empty($x) || empty($y)) {
                    continue;
                }
                $value = $cell['value'];

                $table->saveCell($x, $y, $value);
            }
        }


        return $table;
    }

    /**
     * Удалить таблицу
     *
     * @param Request $request
     * @param Table $table
     * @return void
     * @throws \Exception
     */
    public function destroy(Request $request, Table $table)
    {
        //защита от изменения чужих таблиц
        if ($table->user_id != $request->user()->id) {
            throw new \Exception(403);
        }
        $table->delete();
    }
}
