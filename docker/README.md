Установка composer (не глобально)

    - docker run --rm -v $(pwd):/app composer install

Вдруг нет папки с логами

    - chmod 777 -R ./storage

В папке с проектом 

    - docker-compose up --build -d

Конфигурация Laravel


Добавить в env

    DB_CONNECTION=pgsql
    DB_HOST=postgres
    DB_PORT=5432
    DB_DATABASE=laravel
    DB_USERNAME=postgres
    DB_PASSWORD=root


    - - docker-compose exec php php artisan migrate

    - - docker-compose exec php php artisan key:generate

Чистим Кэш

    - docker-compose exec php php artisan optimize:clear
    






