# Электронные таблицы

![](example.gif)

Использован Laravel Jetstream со стеком Inertia.js + Vue

Функциональность:
* Регистрация / авторизация
* Создание / редактирование / удаление таблиц
* Автоматическое сохранение данных
* Бесконечный скрол как по вертикали так и по горизонтали
* Выбор ячеек мышью - после выбора диапозона предлагается выбор функции (сумма или среднее) для подсчета выбранных ячеек по вертикали и по горизонтали
* Swagger документация, доступна по адресу /api/documentation 

## Структура проекта

Расположение файлов полностью соотвествует принятому в laravel проектах

* **app** - папка с php файлами приложения
* **resources/js** - папка с Vue.js составляющей приложения

## Api 


## Сборка и запуск проекта

```

# Сборка Vue.js
yarn install
yarn run prod

# Сборка контейнера
docker run --rm -v $(pwd):/app composer install
chmod -R 777 storage
docker-compose build --up

```

Приложение будет доступно по адресу http://localhost:81

Управление базой по адресу http://localhost:8080


Доступ:

* HOST=postgres
* PORT=5432
* DATABASE=laravel
* USERNAME=postgres
* PASSWORD=root
