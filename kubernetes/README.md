1. Необходимо поправить файл .env, добавить IP адрес и domain name (если есть) в строку:
**SANCTUM_STATEFUL_DOMAINS=**
2. Далее собираем images. В корне проекта выполнить `docker-compose build`. Если не установлен docker-compose установить. Дожидаемся пока соберуться image. Проверить есть ли собранные новые image. `docker images`.
3. После подключения к kubernetes зайти и поправить конфиги в следующих файлах и после этого применить их 

    `kubectl apply -f app_key.yaml`
    
    `kubectl apply -f db_pass.yaml`

    `kubectl apply -f configmaps/laravel_env_db.yaml`

В зависимости от того где разворачивается база, нужно сконфигурировать верхние файлы пароль от бд находится
db.pass.yaml, за исключением laravel у нее пароль передается php.yaml потому что ларавел принимает его с ошибками из 
секрета.

4. Далее добавляем хранилища для Postgres и Nginx с php
    
    `kubectl apply -f volumes/postgres_volumes_for_storage.yaml`

    `kubectl apply -f volumes/nginx_volumes_for_storage.yaml`

    `kubectl apply -f volumes/storage_volumes_for_php.yaml`


5. Далее необходимо скопировать с проекта файлы. 
    `cp -R public/ /mnt/php`

    `cp -R storage/ /mnt/storage`

    `chown -R www-data:www-data /mnt/public`

    `chown -R www-data:www-data /mnt/php`
    
6. Применяем php.yaml

```
    kubectl apply -f php.yaml
 
```

7. После этого нужно (если были изменения) зайти (после того как запустите php.yaml)  на любой pod php 
```
   
   kubectl exec -it {название пода} /bin/bash

```
и написать 
```
   
   php artisan migrate

```
8. В файле nginx.yaml необходимо прописать внешний IP сервера в параметрах
```
externalIPs:
- <Внешний IP>
```

После применяем nginx.yaml 

```
    kubectl apply -f nginx.yaml

```
9. Файл ingress.yaml применяем, если система большая и состоит минимум из трех нод. Предварительно на Kubernetes проверить настроен ли сервис ingres controler. В случае применения Ingres из файла nginx.yaml убрать строки

```
externalIPs:
- <Внешний IP>
```


Если используем Ingres, то выполняем:

```
    kubectl apply -f ingress.yaml

```
10. Если при обращении к сайту получаем ошибку доступа к БД, то заходим в /mnt/db_data и правим файл pg_hba.configmaps
  Добавляем строку

    `host    all             all              all                    md5`
 
 Сохраняем и перезапускаем под postgres
```
  
   kubectl delete po <название пода>

```
   
